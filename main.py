#!/usr/bin/python

import argparse
from primesieve import *
import sympy
import base64


### Variables ###



## Arguments
parser = argparse.ArgumentParser()
subparser = parser.add_subparsers(dest='command',help='keygen, crypt, decrypt, help')
keygen = subparser.add_parser('keygen')
crypt = subparser.add_parser('crypt')
decrypt = subparser.add_parser('decrypt')

keygen.add_argument('-s', type=int, default=10)
crypt.add_argument('key', type=str)
crypt.add_argument('text', type=str)
crypt.add_argument('-o', type=str)
decrypt.add_argument('key', type=str)
decrypt.add_argument('text', type=str)
decrypt.add_argument('-o', type=str)

args = parser.parse_args()


### Fonctions ###

## Vérifie si le nombre est premier
def is_prime(number):
   if number > 1:
      for a in range(2, number):
         if (number % a) == 0:
            return False
      else:
         return True


## Aide
def help():
    print('\n\nScript monRSA par Alban et Quentin\n\nSyntaxe :\nmonRSA <commande> [<clé>] [<texte>] [switchs]\n\nCommande :\n     keygen : Génére une paire de clé\n     crypt : Chiffre <texte> pour le clé publique <clé>\n     decrypt : Déchiffre <texte> pour le clé privée <clé>\n     help : Affiche ce manuel\nClé :\n     Un fichier qui contient une clé publique monRSA ("crypt") ou une clé privée ("decrypt")\nTexte :\n     Une phrase en clair ("crypt") ou une phrase chiffrée ("decrypt")\n     La pharase chiffré doit être renseigner comme suit : "<bloc1> <bloc2> ... <blocx>"\nSwitchs :\n     -s <size> : Définit la taille de la clé généré par <keygen> (10 par défaut)\n     -o <filename> : Permet de nommer le fichier de sortie pour <crypt> et <decrypt>')


def main(args):
     ## Affiche l'aide si le premier argument n'est pas keygen, crypt, decrypt ou help
     if args.command != 'keygen' and args.command != 'crypt' and args.command != 'decrypt':
         help()

     ## Si le premier argument est keygen
     elif args.command == 'keygen':
         
         ## Minimum et maximum des nombres de x chiffres (correspond au switch -s si présent, sinon 10 par défaut)
         min_x_digit = 10**((args.s)-1) # 10**(x-1)
         max_x_digit = (10**(args.s))-1 # (10**x)-1
         
         ## Tire un nombre premier aléatoire
         p = sympy.randprime(min_x_digit,max_x_digit)

         ## Tire un nombre premier aléatoire différent de q
         while True:
             q = sympy.randprime(min_x_digit,max_x_digit)
             if p!=q:
                 break

         ## Calcul de n et m
         n = p*q
         m = (p-1)*(q-1)
         i = 0

         ## Calcul de e et d
         recherche = True
         while recherche:
             ed = 1 + i * m
             if is_prime(ed) == False:
                 primeed = sympy.primerange(1,ed)
                 for e in primeed :
                     if ed % e == 0 and int(ed/e) != e:
                         d = int(ed/e)
                         recherche = False
                         break
             i +=1

         ## Création du fichier de la clé publique

         file = open("parametre2.pub", "w")
         file.write("---begin monRSA public key---\n")
         file.write(base64.b64encode(bytes(hex(n),'utf-8') + bytes("\n",'utf-8') + bytes(hex(e),'utf-8')).decode('utf-8'))
         file.write("\n---end monRSA key---")
         file.close()

         ## Création du fichier de la clé privée
         file = open("parametre2.priv", "w")
         file.write("---begin monRSA private key---\n")
         file.write(base64.b64encode(bytes(hex(n),'utf-8') + bytes("\n",'utf-8') + bytes(hex(d),'utf-8')).decode('utf-8'))
         file.write("\n---end monRSA key---")
         file.close()

     ## Si le premier argument est crypt    
     elif args.command == "crypt":
         
         ## Ouvre le fichier indiqué dans le deuxième argument et enregistre son contenu dans la variable lines
         file = open(args.key)
         lines = file.readlines()
         file.close()

         ## Vérifie que le fichier ouvert est le bon
         if lines[0].startswith("---begin monRSA public key---"):
             ## Récupère les clés n et e
             a = lines[1]
             a = base64.b64decode(a).decode('utf-8').split('\n')
             n = int(a[0],16)
             e = int(a[1],16)
             message = (args.text) ## Récupère la valeur du troisième argument
             bloc_clair = ""
             block = []
             crypto = []
             taille_bloc = len(str(n))-1
             message = message.replace(' ','').upper() ## Supprimer les espaces et met en MAJ le texte
             ## Affecte chaque lettre du texte à son rang dans l'alphabet 
             for letter in message:
                 ascii = ord(letter) - ord('@')
                 if (len(str(ascii)) == 1):
                     bloc_clair += str(0)+str(ascii)
                 else:
                     bloc_clair += str(ascii)
             ## Si le bloc clair n'est pas pair, rajoute un zéro au début
             while (len(bloc_clair) % (taille_bloc)) !=0 :
                 bloc_clair = str(0) + bloc_clair
             ## Découpe le bloc clair en plusieurs blocs de longueur len(n)-1
             for i in range(0,len(bloc_clair),taille_bloc):
                 block.append(bloc_clair[i:i+taille_bloc])
             ## Pour chaque blocs, on effectue le calcul C = B^e mod n
             for b in block:
                 c = pow(int(b),int(e),int(n))
                 ## Tant que la longueur du bloc n'est pas égale à la longueur len(n)-1, on rajoute des 0 au début
                 while len(str(c)) != len(str(n)):
                        c = str(0) + str(c)
                 crypto.append(str(c)) ## Ajoute le bloc traité à la suite de la variable
             ## Si le switch -o est présent, écrit le cryptogramme dans un fichier, sinon l'affiche
             if args.o:
                 file = open((args.o), "w")
                 file.write(str(crypto))
             else:
                 print("cryptogramme:",crypto)
         ## Affiche si le fichier n'est pas bon
         else:
             print("Erreur, le fichier commence par " + lines[0])

     ## Si le premier argument est decrypt
     elif args.command == "decrypt":
         
         ## Ouvre le fichier indiqué dans le deuxième argument et enregistre son contenu dans la variable lines
         file = open(args.key)
         lines = file.readlines()
         file.close()
         
         ## Vérifie que le fichier ouvert est le bon
         if lines[0].startswith("---begin monRSA private key---"):
             ## Récupère les clés n et d
             a = lines[1]
             a = base64.b64decode(a).decode('utf-8').split('\n')
             n = int(a[0],16)
             d = int(a[1],16)
             taille_bloc = len(str(n))-1
             message = ''
             bloc_clair = ''
             block = ''
             crypto = (args.text) ## Récupère la valeur du troisième argument
             crypto = crypto.split(" ") #Transforme crypto de string à list
             ## Pour chaque blocs, on effectue le calcul B = C^d mod n
             for c in crypto:
                 b = pow(int(c),int(d),int(n))
                 ## Tant que la taille du bloc n'est pas égale à la longueur len(n)-1; ajoute des zéros au début
                 while len(str(b)) < taille_bloc:
                     b = str(0) + str(b)
                 block = block + str(b) ## Ajoute le bloc à la variable
             ## Supprime tous les zéros présents devant la variable
             while block[:1] == "0":
                 block = block[1:]
             ## Tant que la variable n'est pas vide, on prend les deux premiers chiffres qu'on traduit en ASCII
             while len(block) != 0:
                 message = message + chr(int(block[:2])+ord('@'))
                 block = block[2:] ## Supprime les deux premiers chiffres de la variable

             ## Si le switch -o est présent, écrit le cryptogramme dans un fichier, sinon l'affiche    
             if args.o:
                 file = open((args.o), "w")
                 file.write(str(message))
             else:
                 print('message :',message)
                 
         ## Affiche si le fichier n'est pas bon
         else:
             print("Erreur, le fichier commence par " + lines[0])

main(args)
