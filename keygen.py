from primesieve import *
import sympy
import base64

## Variables pour avoir le plus petit et le plus grand nombre de 10 chiffres
min_10_digit = 10**(5-1)
max_10_digit = (10**5)-1

## Vérifie si le nombre est premier
def is_prime(number):
   if number > 1:
      for a in range(2, number):
         if (number % a) == 0:
            return False
      else:
         return True


## Tire un nombre premier aléatoire
p = sympy.randprime(min_10_digit,max_10_digit)

## Tire un nombre premier aléatoire différent de q
while True:
    q = sympy.randprime(min_10_digit,max_10_digit)
    if p!=q:
        break

## Calcul de n et m
n = p*q
m = (p-1)*(q-1)

i = 0

## Calcul de e et d
recherche = True
while recherche:
    ed = 1 + i * m
    if is_prime(ed) == False:
        primeed = sympy.primerange(1,ed)
        for e in primeed :
            if ed % e == 0 and int(ed/e) != e:
                d = int(ed/e)
                recherche = False
                break
    i +=1

## Convertir n, e et d en héxadécimal et encode en base64

## Création du fichier de la clé publique

file = open("parametre2.pub", "w")
file.write("---begin monRSA public key---\n")
file.write(base64.b64encode(bytes(hex(n),'utf-8') + bytes("\n",'utf-8') + bytes(hex(e),'utf-8')).decode('utf-8'))
file.write("\n---end monRSA key---")
file.close()

## Création du fichier de la clé privée

file = open("parametre2.priv", "w")
file.write("---begin monRSA private key---\n")
file.write(base64.b64encode(bytes(hex(n),'utf-8') + bytes("\n",'utf-8') + bytes(hex(d),'utf-8')).decode('utf-8'))
file.write("\n---end monRSA key---")
file.close()
