#!/usr/bin/python
import base64

file = open('parametre2.priv')
lines = file.readlines()
file.close()
#print(lines[0])
if lines[0].startswith("---begin monRSA private key---"):
    a = lines[1]
    a = base64.b64decode(a).decode('utf-8').split('\n')
    n = 452593 
    d = 8513 
    taille_bloc = len(str(n))-1
    message = ''
    bloc_clair = ''
    block = ''
    crypto = ['022720', '209586', '067240', '363982']#['0755', '1324', '2823', '3550', '3763', '2237', '2052']
    print(type(crypto))
    for c in crypto:
        b = pow(int(c),int(d),int(n))
        while len(str(b)) != taille_bloc:
            b = str(0) + str(b)
        block = block + str(b)
    print(block)
    print(len(block))
    while block[:1] == "0":
        block = block[1:]

    while len(block) != 0:
        message = message + chr(int(block[:2])+ord('@'))
        block = block[2:]
        print('message',message)
        print('block',block)

else:
    print("Erreur, le fichier commence par " + lines[0])
